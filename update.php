<?php
include_once("./include/database.php");

if (isset($_GET['id'])) {
    $id = $_GET['id'];
}

$select = $conn->query("SELECT * FROM todo.task WHERE id=$id");
$row = $select->fetch();

// On Update submit change description
if (isset($_POST['submit'])) {
    $description = $_POST['description'];

    $query = "UPDATE todo.task SET description = ' $description ' WHERE id=$id";

    $stmt = $conn->prepare($query);

    // Do query
    $stmt->execute();

    header("location: index.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;600;700;800;900&display=swap" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="./css/style.css">
    <script src="./js/app.js" defer></script>

    <title>Todo List</title>
</head>

<body>
    <header>
        <h1>Update Todo</h1>
    </header>
    <form method="post">
        <input type="text" value="<?php echo $row['description']; ?>" name="description" class="todo-input">
        <button class="todo-button" type="submit" name="submit">
            <i class="fas fa-plus-square"></i>
        </button>
    </form>
</body>

</html>