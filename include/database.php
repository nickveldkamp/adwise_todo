<?php

// Database connection
$servername = "development_programma_database";
$dbname = "todo";
$username = "root";
$password = "";
try {
    // Creating PDO to make DB Connection
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);

    // Error report handling
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Catch PDOException and show them the error message
} catch (PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}
