<?php

include_once './include/database.php';

if (isset($_GET['id'])) {
    $id = $_GET['id'];
}

// Get query and order it
$query = $conn->query("SELECT todo.task.id AS todo_id, todo.task.status_id FROM todo.task, todo.status WHERE todo.task.status_id = todo.status.id ORDER BY todo.task.date_time DESC");

// Looping over to get Status_id
while ($row = $query->fetch()) {
    $status_id = $row['status_id'];
}

// Checks if status is completed. If not then change it.
if ($status_id == 1) {
    $status_id = 2;
} else {
    $status_id = 1;
};


$query = "UPDATE todo.task SET status_id = ' $status_id ' WHERE todo.task.id=$id";

$stmt = $conn->prepare($query);

// Query uitvoeren
$stmt->execute();

// Get back to index page
header("location: index.php");
