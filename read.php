<?php

// Read Database, get tasks, and order it
$query = $conn->query("SELECT todo.task.id AS todo_id, todo.task.description, todo.task.date_time, todo.status.* FROM todo.task, todo.status WHERE todo.task.status_id = todo.status.id ORDER BY todo.task.date_time DESC");

?>

<!-- Loop over tasks and show them on screen -->
<?php while ($row = $query->fetch()) { ?>
    <div class="todo <?php echo ($row['id'] == 1) ? 'completed' : 'uncompleted' ?>" data-id="<?php echo $row['todo_id']; ?>" data-status_id="<?php echo $row['id']; ?>">
        <li class="todo-item"><?php echo $row['description']; ?></li>
        <a href="changeStatus.php?id=<?php echo $row['todo_id']; ?>" class="complete-btn"><i class="fas fa-check"></i></a>
        <a href="update.php?id=<?php echo $row['todo_id']; ?>" class="edit-btn"><i class="fas fa-edit"></i></a>
        <a href="delete.php?id=<?php echo $row['todo_id']; ?>" class="trash-btn"><i class="fas fa-trash"></i></a>
    </div>
<?php } ?>