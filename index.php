<?php

include_once './include/database.php';



if (isset($_POST['submit'])) {



    $description = $_POST['description'];


    $query = "INSERT INTO todo.task (description) VALUES ('$description')";
    // Voer de query uit
    $conn->exec($query);

    header("location: index.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;600;700;800;900&display=swap" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="./css/style.css">
    <script src="./js/app.js" defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <title>Todo List</title>
</head>

<body>
    <header>
        <h1>Nick's Todo List</h1>
    </header>
    <form method="post">
        <input type="text" placeholder="Vul hier uw taak omschrijving in" name="description" class="todo-input">
        <button class="todo-button" type="submit" name="submit">
            <i class="fas fa-plus-square"></i>
        </button>
        <div class="select">
            <select name="todos" class="filter-todo">
                <option value="all">All</option>
                <option value="completed">Completed</option>
                <option value="uncompleted">Uncompleted</option>
            </select>
        </div>
    </form>

    <div class="todo-container">
        <ul class="todo-list"><?php include_once("read.php"); ?></ul>
    </div>
</body>

</html>